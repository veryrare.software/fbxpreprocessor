#if UNITY_EDITOR
using System.IO;
using UnityEditor;
using UnityEditor.AssetImporters;
using UnityEngine;

using System.Collections;
using System.Collections.Generic;
using System;

class FBXPreprocessor : AssetPostprocessor
    {
        static readonly uint k_Version = 1;
        static readonly int k_Order = 3;
        
        public override uint GetVersion()
        {
            return k_Version;
        }

        public override int GetPostprocessOrder()
        {
            return k_Order;
        }

        // to use shared materials
        public Material OnAssignMaterialModel(Material material, Renderer renderer)
        {
            if (material.name.Contains("_shared"))
            {
                // if material exists in assets -> load it and return it
                // else 
                // create asset material at path and load it from assets
                // TODO try load asset if its part of the fbx, else have to create new assets instance
                var assets = AssetDatabase.FindAssets("t:material "+material.name, null);
                if ( assets.Length > 0)
                {
                    Debug.Log("SAME MATERIAL FOUND!" + material.name + " at " + assets[0]);
                    return AssetDatabase.LoadAssetAtPath(assets[0], typeof(Material)) as Material;
                }
            }
            if ( material.name.Contains("_alphaClip"))
            {
                Debug.Log("SET MATERIAL ALPHA CLIP " + material.name);
                //TODO set alpha clipping to the material
            } else if ( material.name.Contains("_alpha") )
            {
                Debug.Log("SET MATERIAL ALPHA " + material.name);
                //TODO set material as transparent
            }
            // else just return default material        
            return null;
        }

        void OnPreprocessModel()
        {
            ModelImporter modelImporter = assetImporter as ModelImporter;
            var assetFolder = assetPath.Substring(0,assetPath.LastIndexOf("/"));
            if (!string.IsNullOrEmpty(path) && !assetFolder.Contains(path)) return;
            if ( modelImporter.ExtractTextures(assetFolder) )
            {
                Debug.Log("FBXPreprocessor : extracted textures from " + assetPath + " to " + assetFolder );
            }

        }

        public void OnPreprocessMaterialDescription(MaterialDescription description, Material material, AnimationClip[] clips)
        {
            // if not URP -> dont preprocess
            // var pipelineAsset = GraphicsSettings.currentRenderPipeline;
            // if (!pipelineAsset || pipelineAsset.GetType() != typeof(UniversalRenderPipelineAsset))
            // {
            //     Debug.LogWarning("FBXPreprocessor works only for URP!");
            //     return;
            // }

            // if not a model -> dont preprocess
            var lowerCaseExtension = Path.GetExtension(assetPath).ToLower();
            if (lowerCaseExtension != ".fbx" && lowerCaseExtension != ".obj" && lowerCaseExtension != ".blend" && lowerCaseExtension != ".mb" && lowerCaseExtension != ".ma" && lowerCaseExtension != ".max")
                return;

            var assetFolder = assetPath.Substring(0,assetPath.LastIndexOf("/"));
            if (!string.IsNullOrEmpty(path) && !assetFolder.Contains(path)) return;
            // List<string> names = new List<string>();
            // description.GetTexturePropertyNames(names);
            // foreach (var name in names)
            // {
            //     Debug.Log("Materialdescription.GetTexturePropertyNames : " + name);
            // }
            TexturePropertyDescription textureProperty;
            if (
                description.TryGetProperty("ReflectionFactor", out textureProperty)
                ||description.TryGetProperty("Metallic", out textureProperty)
                ||description.TryGetProperty("MetallicMap", out textureProperty)
                ||description.TryGetProperty("MetallicSmoothness", out textureProperty)
                ||description.TryGetProperty("MetallicGlossMap", out textureProperty)
                )
            {
                SetMaterialTextureProperty("_MetallicGlossMap", material, textureProperty);
                material.EnableKeyword("_METALLICSPECGLOSSMAP");

            }

            var extension = Path.GetExtension(assetPath);
            var assetName = Path.GetFileNameWithoutExtension(assetPath);
            // Debug.Log("FBXPreprocessor ASSET : " + assetFolder + "   " + assetName + "    " + extension);
            // get all textures in the folder
            var textureAssets = AssetDatabase.FindAssets("t:texture", new string[]{ assetFolder });
            // cover texture GUIDs to AssetPaths 
            textureAssets = Array.ConvertAll(textureAssets, p=>AssetDatabase.GUIDToAssetPath(p));
            // filter only the texture that start with the name of the fbx
            textureAssets = Array.FindAll(textureAssets, p=>Path.GetFileName(p).StartsWith(material.name) || Path.GetFileName(p).StartsWith(assetName));
            // foreach(var t in textureAssets)
            //     Debug.Log("FBXPreprocessor : TEXTURES :" + assetName + " -> "  + AssetDatabase.GUIDToAssetPath(t) );
            
            if ( material.GetTexture("_BaseMap") == null)
            {
                var texPath = Array.Find(textureAssets, x=>Path.GetFileName(x).ToLower().Contains("_albedo")||Path.GetFileName(x).ToLower().Contains("_basecolor")||Path.GetFileName(x).ToLower().Contains("_diffuse"));
                var t = texPath != null ? AssetDatabase.LoadAssetAtPath<Texture2D>(texPath) : null;
                material.SetTexture("_BaseMap", t);
            }
            
            if ( material.GetTexture("_BumpMap") == null)
            {
                var texPath = Array.Find(textureAssets, x=>Path.GetFileName(x).ToLower().Contains("_normal")||Path.GetFileName(x).ToLower().Contains("_bump"));
                var t = texPath != null ? AssetDatabase.LoadAssetAtPath<Texture2D>(texPath) : null;
                material.SetTexture("_BumpMap", t);
            }

            if ( material.GetTexture("_MetallicGlossMap") == null)
            {
                var texPath = Array.Find(textureAssets, x=>Path.GetFileName(x).ToLower().Contains("_metallic"));
                var t = texPath != null ? AssetDatabase.LoadAssetAtPath<Texture2D>(texPath) : null;
                material.SetTexture("_MetallicGlossMap", t);
                if (t != null) material.EnableKeyword("_METALLICSPECGLOSSMAP");
            }

            if ( material.GetTexture("_MetallicGlossMap")!=null)
            {
                material.SetFloat("_Metallic", 1f);
                material.SetFloat("_Smoothness", 1f);
                material.SetFloat("_Glossiness", 1f);
            }
        }

        static void SetMaterialTextureProperty(string propertyName, Material material, TexturePropertyDescription textureProperty)
        {
            material.SetTexture(propertyName, textureProperty.texture);
            material.SetTextureOffset(propertyName, textureProperty.offset);
            material.SetTextureScale(propertyName, textureProperty.scale);
        }


        void OnPreprocessAnimation()
        {
            if (assetPath.Contains("Animation"))
            {
                ModelImporter modelImporter = assetImporter as ModelImporter;

                if (  modelImporter.clipAnimations == null || modelImporter.clipAnimations.Length == 0)
                {
                    var defaultAnimations = modelImporter.defaultClipAnimations;
                    for (int i=0; i <modelImporter.defaultClipAnimations.Length; i++)
                    {
                        if (defaultAnimations[i].name == "mixamo.com" || defaultAnimations[i].name == "" )
                        {
                            var modelname = Path.GetFileNameWithoutExtension(assetPath);
                            var ns = modelname.Split("@");
                            defaultAnimations[i].name = ns[ns.Length-1];
                        }
                    }
                    modelImporter.clipAnimations = defaultAnimations;
                }

                var animations = modelImporter.clipAnimations;
                for (int i=0; i <animations.Length; i++)
                {
                    if (animations[i].name == "mixamo.com" || animations[i].name == "" )
                    {
                        var modelname = Path.GetFileNameWithoutExtension(assetPath);
                        var ns = modelname.Split("@");
                        animations[i].name = ns[ns.Length-1];
                    }
                    var n = animations[i].name.ToLower();
                    // looping
                    if (n.Contains("loop") || n.Contains("idle") || ((n.Contains("walk") || n.Contains("run")) && !n.Contains("stop") && !n.Contains("start")))
                    {
                        animations[i].loopTime = true;
                    }
                    // todo baking
                }
                modelImporter.clipAnimations = animations;
            }
        }



        // ------- setup -------
        public static string path = "Content/_Models";

        [InitializeOnLoadMethod]
        static void GetFromPlayerPref()
        {
            if ( EditorPrefs.HasKey("FBXPreprocessorPath"))
                path = EditorPrefs.GetString("FBXPreprocessorPath");
            else {
                Debug.Log("FBXPreprocessorPath missing in EditorPrefs, fallback to : Content/_Models");
            }
        }
    }


public class FBXPlayerPrefsWindow : EditorWindow
{
    // Add menu named "My Window" to the Window menu
    [MenuItem("Custom/FBXPlayerPrefsWindow")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        FBXPlayerPrefsWindow window = (FBXPlayerPrefsWindow)EditorWindow.GetWindow(typeof(FBXPlayerPrefsWindow));
        window.Show();
    }

    void OnGUI()
    {
        GUILayout.Label("Base Settings", EditorStyles.boldLabel);
        FBXPreprocessor.path = EditorGUILayout.TextField("FBXPreprocessor.path", FBXPreprocessor.path );
        EditorPrefs.SetString("FBXPreprocessorPath", FBXPreprocessor.path);
    }
}
#endif
